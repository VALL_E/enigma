import numpy as np
import pandas as pd

try:
	from atools import ATools

	print("----------------------Проверка на создание словаря ATools----------------------")
	WFt = ATools()
	WFt.addtool("#1", "Select")
	WFt.addtool("#2", "Select")
	WFt.addtool("#3", "Select")
	WFt.addtool("#4", "Select")
	WFt.addtool("#5", "Select")
	WFt.addtool("#6", "Select")
	print(WFt.tools)
	print("-----------------Проверка на создание словаря ATools завершена-----------------")
	print('\n')

	print("----Получаем из словаря ATools элемент по индексу '''#6''' как объект Obj------")
	print(WFt.tools["#1"])
	print(WFt.tools["#2"])
	print('\n')

	print("-------Из родительского объекта WFt запрашиваем свойтво calculation_mode-------")
	WFt.calculation_mode = 1
	print(WFt.calculation_mode)
	print('\n')

	print("------------------------Получаем доступ к полям Select-------------------------")
	Obj0 = WFt.tools["#5"]
	Obj = WFt.tools["#6"]
	print("inputToolID:", Obj.inputToolID,
		  "inputTool:", Obj.inputTool,
		  "inputDF:",Obj.inputDF,
		  "selection_settings_DF:", Obj.selection_settings_DF)
	print('\n')

	inputDF = pd.DataFrame(
	{
		"A": ["A0", "A1", "A2", "A3"],
		"B": ["B0", "B1", "B2", "B3"],
		"C": ["C0", "C1", "C2", "C3"],
		"D": ["D0", "D1", "D2", "D3"],
	},
	index = [0, 1, 2, 3],)

	selection_settings_DF = pd.DataFrame(
	{
		"field": ["*Unknown"],
		"selected": [True],
		"rename": [np.nan],
		"type": [np.nan],
		"size": [np.nan],
		"description": [np.nan],
	},
	index=["***Unknown"],)

	Obj.fill_input("#99", Obj0, inputDF, selection_settings_DF)
	print("inputToolID:", Obj.inputToolID, '\n',
		  "inputTool:", Obj.inputTool, '\n',
		  "inputDF:", '\n', Obj.inputDF, '\n',
		  "selection_settings_DF:", '\n', Obj.selection_settings_DF)
	print('\n')

except Exception as e:
	print("Exception:" + str(e))
	raise
else:
	pass
finally:
	pass
