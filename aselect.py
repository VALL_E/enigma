import numpy as np
import math

#from mfunc import MF


class ASelect:
    """Select tool"""

    def __init__(self, selfID):
        """Constructor"""
        try:
            self.atype = "Select"
            self.aID = selfID
            self.inputToolID = np.nan
            self.inputTool = np.nan
            self.selection_settings_DF = ""
            self.inputDF = ""
            self.outputDF = ""
            self.outputToolID = {}
            self.outputTool = {}
        except Exception as e:
            self.Exception = str(e)
            raise
        else:
            pass
        finally:
            pass

    def __del__(self):
        class_name = self.__class__.__name__
        # print('{} уничтожен'.format(class_name))

    def clear_input(self):
        clear_custom_input(self)

    def fill_input(self, inputToolID, inputTool, inputDF, selection_settings_DF):
        fill_custom_input(self, inputToolID, inputTool, inputDF, selection_settings_DF)

    def fill_output(self, outputDF):
        fill_custom_output(self, outputDF)

    def clear_output(self):
        clear_custom_output(self)

    def GetOutputUnit(self, inputToolID):
        """вызов функции из родительского класса дающей ссылку на объект по его ID"""
        if calculation_mode == 1:
            pass
        elif calculation_mode == 0:
            pass

    def getmy(self):
        return self


def isNaN(num):
    return num!= num

def clear_custom_input(self):
    self.inputToolID = np.nan
    self.inputTool = np.nan
    self.selection_settings_DF = ""
    self.inputDF = ""

def GO(self):
    """do local logical constructions"""
    result = np.nan #MF.SelectDF(self.selection_settings_DF, self.inputDF)
    self.fill_output(result)

def fill_custom_input(self, inputToolID, inputTool, inputDF, selection_settings_DF):
    self.inputToolID = inputToolID
    self.inputTool = inputTool
    # self.selection_settings_DF = MF(selection_settings_DF)
    self.selection_settings_DF = selection_settings_DF
    self.inputDF = inputDF
    # if not isNaN(self.inputToolID) and not isNaN(self.inputTool) and isNaN(self.selection_settings_DF) and isNaN(self.inputDF):
    if not isNaN(self.inputToolID) and not isNaN(self.inputTool):
        GO

def induce_inheritor_input(self, induceDF, utool):
    """IN DEF WE RUN THE LOGIC INDUCING THE INHERITOR"""
    utool.fill_input(self.aID, self, induceDF, self.selection_settings_DF)

def fill_custom_output(self, outputDF):
    self.outputDF = outputDF
    for ToolID in self.outputToolID:
        cur_utool = self.outputTool[ToolID]
        induce_inheritor_input(outputDF, cur_utool)

def clear_custom_output(self):
    self.outputDF = ""
    self.outputToolID.clear()
    self.outputToolID = np.nan
    self.outputTool.clear()
    self.outputTool = np.nan

