import numpy as np
import pandas as pd


class MF:
    """Metafield"""


def templating_selection_settings_DF(selection_settings_DF):
    """templating selection dataframe"""
    base_field_attributes_DF = pd.DataFrame(
        {
            "field": ["*Unknown"],
            "selected": [True],
            "rename": [np.nan],
            "type": [np.nan],
            "size": [np.nan],
            "description": [np.nan],
        },
        index=["***Unknown"],
    )

    df2 = selection_settings_DF
    """df2 = selection_settings_DFpd.DataFrame(
        {
        "field": ["field1", "field2", "field3", "*Unknown"],
        "selected": [True, True, True, True],
        "rename": ["f1", "f2", "f3", None],
        "type": ["WString", "V_String", "V_WString", None],
        "size": [1, 2, 3, None],
        #"description": ["jopa", "rassia", "dollar", "drebeden'"],
        },
        index=["field1", "field2", "field3", "*Unknown"],
        )"""

    # df = pd.concat([df2, df1], ignore_index=False, sort=False)
    # df = result.copy()
    df = df2.append(df1, ignore_index=False, sort=False)
    result = df.reindex(df2.index)
    return result

    def __init__(self, selection_settings_DF):
        """Constructor"""

    try:
        self = templating_selection_settings_DF(selection_settings_DF)
    except Exception as e:
        self.Exception = str(e)
        raise
    else:
        pass
    finally:
        pass


def SelectDF(self, selection_settings_DF, DF):
    """Select dataframe"""

    # хороший кейс переименования столбцов
    # https://coderoad.ru/11346283/%D0%9F%D0%B5%D1%80%D0%B5%D0%B8%D0%BC%D0%B5%D0%BD%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D1%81%D1%82%D0%BE%D0%BB%D0%B1%D1%86%D0%BE%D0%B2-%D0%B2-Pandas
    # old_names = ['$a', '$b', '$c', '$d', '$e']
    # new_names = ['a', 'b', 'c', 'd', 'e']
    # df.rename(columns=dict(zip(old_names, new_names)), inplace=True)
    # pd.concat([c for _, c in df.items()], axis=1, keys=new)
