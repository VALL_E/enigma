import numpy as np

# from utools import UniTools
from aselect import ASelect

class ATools:
    """Create a Dict of Universal Tools"""

    def __init__(self):
        """Constructor"""
        try:
            self.tools = {}
            self.calculation_mode = 0  #0 - reconciliation/1 - computation
        except Exception as e:
            self.Exception = str(e)
            raise
        else:
            pass
        finally:
            pass

    def addtool(self, tID, ttype):
        """add new Universal Tool"""
        try:
            # self.tools[tID] = UniTools(self, tID, ttype)
            if ttype == "Select":
                self.tools[tID] = ASelect(tID)
            elif ttype == "add AnyTypes":
                pass
            else:
                pass
        except Exception as e:
            self.Exception = str(e)
            self.tools[tID] = self.Exception
            raise
        else:
            pass
        finally:
            pass


