#import mfunc
import numpy as np
import pandas as pd
import os
import sys


def test():

    try:

        '''df1 = pd.DataFrame(
            {
            "A": ["A0", "A1", "A2", "A3"],
            "B": ["B0", "B1", "B2", "B3"],
            "C": ["C0", "C1", "C2", "C3"],
            "D": ["D0", "D1", "D2", "D3"],
            },
            index=[0, 1, 2, 3],)

        df2 = pd.DataFrame(
            {
            "A": ["A4", "A5", "A6", "A7"],
            "B": ["B4", "B5", "B6", "B7"],
            "C": ["C4", "C5", "C6", "C7"],
            "D": ["D4", "D5", "D6", "D7"],
            },
            index=[4, 5, 6, 7],)

        df3 = pd.DataFrame(
            {
            "A": ["A8", "A9", "A10", "A11"],
            "B": ["B8", "B9", "B10", "B11"],
            "C": ["C8", "C9", "C10", "C11"],
            "D": ["D8", "D9", "D10", "D11"],
            },
            index=[7, 9, 10, 11],)

        df4 = pd.DataFrame(
            {
            "B": ["B2", "B3", "B6", "B7"],
            "D": ["D2", "D3", "D6", "D7"],
            "F": ["F2", "F3", "F6", "F7"],
            },
            index=[2, 3, 6, 7],)

        #frames = [df1, df2, df3]
        #result = pd.concat(frames,0,'outer',ignore_index=False,keys=["z", "y", "x"])
        #result = pd.concat([df1, df4], axis=1)
        #result = pd.concat([df1, df4], axis=1, join="inner")
        #result = pd.concat([df1, df4], axis=1).reindex(df1.index)
        #result = pd.concat([df1, df4.reindex(df1.index)], axis=1)
        #result = df1.append(df2)
        #result = df1.append(df4, sort=False)'''



        df1 = pd.DataFrame(
            {
            "field": ["*Unknown"],
            "selected": [True],
            "rename": [np.nan],
            "type": [np.nan],
            "size": [np.nan],
            "description": [np.nan],
            },
            index=["***Unknown"],
            )

        df2 = pd.DataFrame(
            {
            "field": ["field1", "field2", "field3", "*Unknown"],
            "selected": [True, True, True, True],
            "rename": ["f1", "f2", "f3", np.nan],
            "type": ["WString", "V_String", "V_WString", np.nan],
            "size": [1, 2, 3, np.nan],
            #"description": ["jopa", "rassia", "dollar", "drebeden'"],
            },
            index=["field1", "field2", "field3", "*Unknown"],
            )
        #result = pd.concat([df1, df2], axis=0, join="outer", verify_integrity=True)
        #result = df1.append(df2, ignore_index=True)
        #result = df1.append(df2, sort=False)
        #result = df2.reindex(df1.index)
        #result = df1.append(df2, sort=False)
        #result = df1.reindex(df1.index)

        df = pd.concat([df2, df1], ignore_index=False, sort=False)
        #df = result.copy()
        df = df2.append(df1, ignore_index=False, sort=False)
        result = df.reindex(df2.index)

        # https://pandas.pydata.org/pandas-docs/stable/user_guide/basics.html
        # этот подход тоже пригоден, но в df1 нужно заменить на index=["*Unknown"]. Сбивает порядок полей и работает быстрее.
        #result = df2.combine_first(df1)

    except Exception as e:
        return ("Exception:" + str(e))
        raise
    else:
        pass
    finally:
        return result


print(test())